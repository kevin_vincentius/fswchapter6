const express = require("express")
const { sequelize, user_game, user_game_history, user_game_biodata } = require("./models");
const bodyParser = require("body-parser")

const app = express();
app.use(express.json());

app.set("view engine", "ejs")
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static("public"))

app.get("/", async (req, res) => {
  const error  = null
  res.render("login", { error })
})

//login
app.post("/login", async (req, res) => {
  const { username, password } = req.body;

  const user = await user_game.findOne({where: { username, password, role: "superadmin"}})

  if(user){
    if(password == user.password && user.role){
      user_game.findAll()
      .then((userGame) => {
        res.render("dashboard", { user_game: userGame } )
      })
    }
  } 
  else{
    const error = "invalid username/ password"
    res.render("login", { error })
  }
});

//dashboard
app.get("/dashboard", async (req, res) => {
  user_game.findAll()
  .then((userGameData) => {
    res.render("dashboard", { user_game: userGameData} )
  })
})


// create
app.get("/dashboard/create", async (req, res) => {
  res.render("create")
})

app.post("/dashboard/create", async (req, res) => {
  try{
    const usergame = await user_game.create({
      username: req.body.username,
      password: req.body.password,
      role: req.body.role,
  })
    const usergamebiodata = await user_game_biodata.create({
      id: usergame.id,
      uuid: usergame.uuid,
      username: usergame.username,
      nama: req.body.nama,
      email: req.body.email,
      gender: req.body.gender,
      date_of_birth: req.body.date_of_birth,
      country_of_birth: req.body.country_of_birth,
   })
    await user_game_history.create({
      id: usergame.id,
      uuid: usergame.uuid, 
      username: usergame.username,
      nama: usergamebiodata.nama
    })

    res.redirect('/dashboard')
    
   }
   catch (err){
    console.log(err)
    res.send("something went wrong")
  }
})


//update
app.get("/dashboard/update/(:id)", async (req, res) => {
  const usergame = await user_game.findOne({ where: { id: req.params.id }})
  const usergamebiodata = await user_game_biodata.findOne({ where: { id: req.params.id }})

  res.render("update", { 
    id: req.params.id,
    username: usergame.username,
    nama: usergamebiodata.nama,
    email: usergamebiodata.email, 
    password: usergame.password,
    role: usergame.role,
    gender: usergamebiodata.gender,
    date_of_birth: usergamebiodata.gender,
    country_of_birth: usergamebiodata.country_of_birth,
    user_game,
    user_game_biodata
  })
})

app.post("/dashboard/update/(:id)", async (req,res) => {
  user_game.update(
    {
      username: req.body.username,
      password: req.body.password,
      role: req.body.role,
    },
    {
      where: { id: req.params.id },
    }
  ), user_game_biodata.update(
    {
      username: req.body.username,
      nama: req.body.nama,
      email: req.body.email,
      gender: req.body.gender,
      date_of_birth: req.body.date_of_birth,
      country_of_birth: req.body.country_of_birth
    },
    {
      where: { id: req.params.id },
    }
  ) 
  user_game_history.update(
    {
      username: req.body.username,
      nama: req.body.nama,
    },
    {
      where: { id: req.params.id },
    }
  ) 
   .then(() => {
      res.redirect("/dashboard")
    })
    .catch((err) => {
      res.send("Can't update the user data");
      console.log(err)
    });
})


//delete
app.get("/dashboard/delete/(:id)", (req, res) => {
  user_game.destroy({
    where: { id: req.params.id },
  }),
  user_game_biodata.destroy({
    where: { id: req.params.id }
  }),
  user_game_history.destroy({
    where: { id: req.params.id }
  })
  .then(() => {
    res.redirect("/dashboard")
    })
  });


app.get("/user-game", (req, res) => {
  user_game.findAll().then((userGame) => {
    res.render("user-game", { user_game: userGame })
  })
})

app.get("/user-game-biodata", (req, res) => {
  user_game_biodata.findAll().then((userGameBiodata) => {
    res.render("user-game-biodata", { user_game_biodata: userGameBiodata })
  })
})

app.get("/user-game-history", (req, res) => {
  user_game_history.findAll().then((userGameHistory) => {
    res.render("user-game-history", { user_game_history: userGameHistory })
  })
})


app.listen({ port: 3000 }, async () => {
    console.log("Server up on htpp://localhost:3000");
    await sequelize.authenticate();
  
    console.log("Database Connected!");
  });
  
