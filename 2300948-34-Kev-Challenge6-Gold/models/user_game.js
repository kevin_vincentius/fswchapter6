'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    
    static associate({ user_game_history }) {
      // define association here

      this.hasOne(user_game_history, { 
        foreignKey: "username",
      });


    }

    
    static associate({ user_game_biodata }) {
      // define association here
      
      this.hasOne(user_game_biodata, { 
        foreignKey: "uuid",
      });

      this.hasOne(user_game_biodata, { 
        foreignKey: "username",
      });
    }
  }
  user_game.init({
    uuid: {
      type: DataTypes.STRING,
      defaultValue: DataTypes.UUIDV4,
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    role: {
      type: DataTypes.STRING,
      allowNull:false
    }
  }, {
    sequelize,
    tableName: "user_game",
    modelName: 'user_game',
  });

  return user_game;
};



