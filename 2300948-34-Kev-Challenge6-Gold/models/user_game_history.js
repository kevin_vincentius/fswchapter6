'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ user_game }) {
      // define association here

      this.belongsTo(user_game, { 
        foreignKey: "username",
      });

     
    }

    static associate({ user_game_biodata }) {
      // define association here

      this.belongsTo(user_game_biodata, { 
        foreignKey: "nama"
      });

      this.belongsTo(user_game_biodata, { 
        foreignKey: "uuid",
      });
    }
  }
  user_game_history.init({
    // uuid: {
    //   type: DataTypes.STRING,
    //   defaultValue: DataTypes.UUIDV4,
    // },
    username: {
      type: DataTypes.STRING,
      allowNull:false
    },
    nama: {
      type: DataTypes.STRING,
      allowNull:false
    },
    score: DataTypes.STRING,
    level: DataTypes.INTEGER,
    last_saved_on: DataTypes.DATE,
    points: DataTypes.INTEGER,
    hours_played: DataTypes.INTEGER,
  }, {
    sequelize,
    tableName: "user_game_history",
    modelName: 'user_game_history',
  });
  return user_game_history;
};